import java.util.LinkedList;

public class LongStack {
   private LinkedList<Long> myLinkedList;

   public static void main(String[] args) {
//        LongStack m = new LongStack();
//        System.out.println(m);
//        m.push(2);
//        System.out.println(m);
//        m.push(5);
//        System.out.println(m);
//        m.push(9);
//        System.out.println(m);
//        m.push(2);
//        System.out.println(m);
//        m.op("ROT");
//        System.out.println(m);
//        m.op("*");
//        System.out.println(m);
//        m.op("-");
//        System.out.println(m);
//        long tulemus = m.pop();
//        System.out.println(m);
//        System.out.println(tulemus);
//        LongStack acopy = m;
//        System.out.println(acopy.equals(m));
//        System.out.println(m);
//        System.out.println(acopy);
//        try {
//            acopy = (LongStack) m.clone();
//        } catch (CloneNotSupportedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(acopy.equals(m));
//        System.out.println(m);
//        System.out.println(acopy);
//        m.push(6);
//        System.out.println(acopy.equals(m));
//        System.out.println(m);
//        System.out.println(acopy);
//        m.pop();
//        System.out.println(acopy.equals(m));
//        System.out.println(m);
//        System.out.println(acopy);
//        String prog = "2 3 + 4 * 10 /";
//        if (args.length > 0) {
//            StringBuffer sb = new StringBuffer();
//            for (int i = 0; i < args.length; i++) {
//                sb.append(args[i]);
//                sb.append(" ");
//            }
//            prog = sb.toString();
//        }
//        System.out.println(prog + "\n "
//                + String.valueOf(LongStack.interpret(prog)));
//        System.out.println(String.valueOf(LongStack.interpret("1  2    +")));
//        System.out.println(String.valueOf(LongStack.interpret("2 3 + 7")));
//        System.out.println(String.valueOf(LongStack.interpret("2 5 SWAP -")));
//        System.out.println(String.valueOf(LongStack.interpret("2 5 9 ROT - +")));
      System.out.println(String.valueOf(LongStack.interpret("2 5 9 ROT + SWAP -")));
   }

   LongStack() {
      // TODO!!! Your constructor here!
      myLinkedList = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack tmp = new LongStack();
      if (!stEmpty())
         for (Long aLong : myLinkedList) {
            tmp.myLinkedList.addLast(aLong);
         }
      return tmp;
   }

   public boolean stEmpty() {
      return myLinkedList.isEmpty();// TODO!!! Your code here!
   }

   public void push(long a) {
      // TODO!!! Your code here!
      myLinkedList.push(a);
   }

   public long pop() {
      if (stEmpty())
         throw new IndexOutOfBoundsException(" stack underflow");
      return myLinkedList.pop(); // TODO!!! Your code here!
   } // pop

   public void op(String s) {
      if (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/") || s.equals("SWAP") || s.equals("ROT")) {
         if (myLinkedList.size() < 2)
            throw new IndexOutOfBoundsException(" too few elements for " + s);
         long op2 = pop();
         long op1 = pop();
         if (s.equals("+"))
            push(op1 + op2);
         else if (s.equals("-"))
            push(op1 - op2);
         else if (s.equals("*"))
            push(op1 * op2);
         else if (s.equals("SWAP")){
            push(op2);
            push(op1);
         }
         else if (s.equals("ROT")){
            if (myLinkedList.size() < 1)
               throw new IndexOutOfBoundsException(" too few elements for " + s);
            long op3 = pop();
            push(op1);
            push(op2);
            push(op3);
         }
         else
            push(op1 / op2);

      } else {
         throw new IllegalArgumentException("Invalid operation: " + s);
      }
   }

   public long tos() {
      if (stEmpty())
         throw new IndexOutOfBoundsException(" stack underflow");
      return myLinkedList.getFirst();
   }

   @Override
   public boolean equals(Object o) {
      if (((LongStack) o).myLinkedList.size() != myLinkedList.size())
         return false;
      for (int i = 0; i <= myLinkedList.size() - 1; i++)
         if (((LongStack) o).myLinkedList.get(i) != myLinkedList.get(i))
            return false;
      return true;
   }

   @Override
   public String toString() {
      if (stEmpty())
         return "empty";
      StringBuffer b = new StringBuffer();
      for (int i = 0; i <= myLinkedList.size() - 1; i++)
         b.append(String.valueOf(myLinkedList.get(i)) + " ");
      b.reverse();
      return b.toString();
   }

   public static long interpret(String pol) {
      if (pol.equals("")) {
         throw new IllegalArgumentException("Empty expression");
      }
      LongStack newLongStack = new LongStack();
      for (String s : pol.split("\\s+")) {
         if (s.equals("")) continue;
         try {
            long d = Long.parseLong(s);
            newLongStack.push(d);
         } catch (NumberFormatException nfe) {
            try {
               newLongStack.op(s);
            } catch (IllegalArgumentException e) {
               throw new RuntimeException("Illegal symbol " + s + " in expression \"" + pol + "\" ");
            } catch (IndexOutOfBoundsException es) {
               throw new RuntimeException("Cannot perform " + s + " in expression \"" + pol + "\" ");
            }
         }
      }
      if (newLongStack.myLinkedList.size() > 1) {
         throw new RuntimeException("Too many numbers in expression \"" + pol + "\" ");
      }
      return newLongStack.tos();
   }

}

